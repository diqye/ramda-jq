! function(exports, $, r) {
    var curry = r.curry;

    function jq(fn) {
        return function(e) {
            return fn($(this), e);
        };
    }

    function jqpipe() {
        return jq(r.pipe.apply(null, arguments));
    }

    function fntor(name, alen) {
        switch (alen) {
            case 0:
                return function(el) {
                    return el == null ? null : el[name]();
                };
            case 1:
                return function(a1, el) {
                    return el == null ? null : el[name](a1);
                };
            case 2:
                return function(a1, a2, el) {
                    return el == null ? null : el[name](a1, a2);
                };
            case 3:
                return function(a1, a2, a3, el) {
                    return el == null ? null : el[name](a1, a2, a3);
                };
            case 4:
                return function(a1, a2, a3, a4, el) {
                    return el == null ? null : el[name](a1, a2, a3, a4, a5);
                };
            case 5:
                return function(a1, a2, a3, a4, a5, el) {
                    return el == null ? null : el[name](a1, a2, a3, a4, a5);
                };
        }
        throw "最多5个参数";
    }

    function fntor0(name) {
        return curry(fntor(name, 0));
    }

    function fntor1(name) {
        return curry(fntor(name, 1));
    }

    function fntor2(name) {
        return curry(fntor(name, 2));
    }
    var attr = fntor1("attr");
    var setAttr = fntor2("attr");
    var attrs = curry(function(names, el) {
        return r.map(attr(r.__, el), names);
    });
    var removeClass = fntor1("removeClass");
    var addClass = fntor1("addClass");
    var each = fntor1("each");
    var on = fntor2("on");
    var once = fntor2("once");
    var find = fntor1("find");
    var hover = fntor2("hover");
    var show = fntor0("show");
    var hide = fntor0("hide");

    var hoverUp = curry(function _hoverUp(delay, fn1, fn2, el) {
        var timeout = null;
        return hover(function() {
            clearTimeout(timeout);
            fn1(el);
        }, function() {
            timeout = setTimeout(function() {
                fn2(el);
            }, delay);
        }, el);
    });

    var hoverToogle = r.curry(function(delay, fn, el) {
        el.each(jq(hoverUp(delay, r.pipe(fn, show), r.pipe(fn, hide))));
        return el;
    });

    var hoverToogle500 = hoverToogle(500);

    exports.ui = {
        jq: jq,
        jqpipe: jqpipe,
        attr: attr,
        attrs: attrs,
        setAttr: setAttr,
        on: on,
        once: once,
        removeClass:removeClass,
        addClass:addClass,
        find: find,
        show: show,
        hide: hide,
        hover: hover,
        hoverUp: hoverUp,
        hoverToogle: hoverToogle,
        hoverToogle500: hoverToogle500
    };
}(this, $, R);
